#coding: utf-8

import re
from unicodedata import normalize, category

from django.template import defaultfilters

from pipeline.compilers import Compiler

VOIVODESHIPS = (
   u'dolnośląskie',
   u'kujawsko-pomorskie',
   u'lubelskie',
   u'lubuskie',
   u'łódzkie',
   u'małopolskie',
   u'mazowieckie',
   u'opolskie',
   u'podkarpackie',
   u'podlaskie',
   u'pomorskie',
   u'śląskie',
   u'świętokrzyskie',
   u'warmińsko-mazurskie',
   u'wielkopolskie',
   u'zachodniopomorskie',
)

VOIVODESHIP_LOWER_SILESIAN_= 0
VOIVODESHIP_KUYAVIAN_ = 1
VOIVODESHIP_LUBLIN = 2
VOIVODESHIP_LUBUSZ = 3
VOIVODESHIP_LODZ = 4
VOIVODESHIP_LESSER_POLAND = 5
VOIVODESHIP_MASOVIAN = 6
VOIVODESHIP_OPOLE = 7 
VOIVODESHIP_SUBCARPATHIAN = 8 
VOIVODESHIP_PODLASKIE = 9 
VOIVODESHIP_POMERANIAN = 10
VOIVODESHIP_SILESIAN = 11 
VOIVODESHIP_SWIETOKRZYSKIE = 12 
VOIVODESHIP_WARMIAN_MASURIAN = 13
VOIVODESHIP_GREATER_POLAND = 14
WVOIVODESHIP_WEST_POMERANIAN = 15

VOIVODESHIP_CHOICES = list(enumerate(VOIVODESHIPS))

def compile_js(file_list):
    """ Compiles js files. They should be written in
    CoffeeScript. CoffeeScriptCompiler should be enabled.
    """
    c = Compiler()
    return c.compile(file_list)

def slugify(value):
    return defaultfilters.slugify(re.sub(u"ł", "l", re.sub(u"Ł", "l", value)))

def norm_text_unicode(val):
    """Converts unicode to standard ascii e.g. mała -> mala
    """
    return filter(lambda c: category(c) != 'Mn', normalize('NFKD', val)).replace(u'ł', u'l').replace(u'Ł', u'L')
