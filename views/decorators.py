#coding: utf-8

import time
import datetime

try:
    import simplejson as json
except ImportError:
    import json

from django.core.serializers.json import DjangoJSONEncoder
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse

def expire_in(**kwargs):
    def renderer(func):
        def wrapper(request, *args, **kw):  
            response = func(request, *args, **kw)
            hours_time_diff = time.altzone/3600
            hours_time_diff = "+%02d"%abs(hours_time_diff)
            
            response['Expires'] = (datetime.datetime.today() + datetime.timedelta(**kwargs))\
                .strftime('%a, %d %b %Y %H:%M:%S ' + '%s00'%(hours_time_diff,))
            return response
        return wrapper
    return renderer

def render_with(template, logged_in_template=None, headers=None):
    """
    Decorator for Django views that sends returned dict to render_to_response function
    with given template and RequestContext as context instance.
    You can pass headers as dictionary
    Example:
    
    @render_with('f/template.html', headers={'Cache-Control': 'no-cache'})
    def f(request):
        return {}
    """
    def renderer(func):
        def wrapper(request, *args, **kw):
            template_to_render = logged_in_template if logged_in_template and \
                request.user.is_authenticated() else template      
            output = func(request, *args, **kw)
            response_modifiers = {}
            if output.__class__ is tuple:
                if len(output) > 1:
                    response_modifiers = output[1]
                output = output[0]
            if isinstance(output, dict):        
                response = render_to_response(
                    template_to_render, output,
                    RequestContext(request, output)
                )
                for key, val in response_modifiers.items():
                    setattr(response, key, val)
                if headers:
                    for h_name, h_val in headers.items():
                        response[h_name] = h_val
                return response
            return output
        return wrapper
    return renderer
        
def json_response(func):
    """
    Use it to decorate view. The view must return value which can be json - dumped    
    
    @json_response
    def test(request):
        return {"i1": 12, "i2": 13}
    """
    
    def wrapper(request, *args, **kw):
        output = func(request, *args, **kw)

        return HttpResponse(json.dumps(output, cls=DjangoJSONEncoder),
                            mimetype='application/json')

    return wrapper



def render_with_formats(**decorator_kwargs):
    """
    Decorator for Django views that helps with render view in various formats.
    
    Possible kwargs:
        -  format name
           Any format name like: html, xml, pdf, ...
        - `format_param_name`, defult = `format`
        - `default_format`, default = `html`

    
    Format will be determine from:
        - view kwargs
          default key is `format`, can be change by decorator kwarg `format_param_name`
        - request GET prams
          default param is the same like in view kwargs
    
    
    Format must have config defined. There is only one predefined config, for html.
    Config dict should be passed decorator in kwargs. Key word is format name.
    Example:
        @render_with_formats(pdf={'renderer': pdf_renderer_func}, html={'template': 'foo/tpl.html')
        def f(request):
            return {}
    
    Possible config params:
        - renderer
          Function which returns complete response from context etc.
          Example:
              def pdf_renderer_func(request, view_output, config, view_args, view_kwargs):
                  return create_pdf_response(view_output)
        - before_call
          Callback which will be call before view.
          Example:
              def before_view(request, config, view_args, view_kwargs):
                  do something
        - after_call
          Like `before_call` but is call after view function call
          and got one more argument `view_output`.
          
    """
    
    def _render_format_html(request, **kwargs):
        tpl = kwargs['config'].get('template', None)
        if not tpl:
            tpl = kwargs['view_output'].get('template', None)
        
        if not tpl:
            raise ValueError('Template is not provided')
        
        return render_to_response(tpl, kwargs['view_output'],
                                  RequestContext(request, kwargs['view_output']))
    
    DEFAULT_RENDERER = _render_format_html
    
    FORMAT_DEFAULTS = {
        'renderer': DEFAULT_RENDERER,
        'before_call': lambda *args, **kwargs: None,
        'after_call': lambda *args, **kwargs: None,
    }
    
    FORMATS = {
        'html': {
            'renderer': DEFAULT_RENDERER,
            'template': None,
        },
    }
    
    FORMAT_PARAM_NAME = decorator_kwargs.pop('format_param_name', 'format')
    DEFAULT_FORMAT = decorator_kwargs.pop('default_format', 'html')
    
    def renderer(func):
        def wrapper(request, *args, **kwargs):
            # Try to find request format in view kwargs or in GET param
            req_format = kwargs.get(FORMAT_PARAM_NAME,
                                    request.GET.get(FORMAT_PARAM_NAME, DEFAULT_FORMAT)).lower()
            
            # Check if format config exists
            if req_format not in FORMATS and req_format not in decorator_kwargs:
                raise ValueError('Configuration for format \'%s\' not found' % req_format)
            
            # Prepare current format config from defaults and decorator kwargs
            format_config = FORMAT_DEFAULTS.copy()
            format_config.update(FORMATS.get(req_format, {}))
            format_config.update(decorator_kwargs.get(req_format, {}))
            
            callbacks_kwargs = {'config': format_config, 'view_args': args, 'view_kwargs': kwargs}
            
            # Call before view callback
            format_config['before_call'](request, **callbacks_kwargs)
            
            # Call view
            output = func(request, *args, **kwargs)
            
            # Call before view callback
            format_config['after_call'](request, view_output=output, **callbacks_kwargs)
            
            # If view returns complete HTTP response
            if isinstance(output, HttpResponse):
                return output
            
            # If view returns dict
            if isinstance(output, dict):
                response = format_config['renderer'](request, view_output=output, **callbacks_kwargs)
                headers = format_config.get('headers', {})
                headers.update(output.get('headers', {}))
                for header_name, header_value in headers.items():
                    response[header_name] = header_value
                return response
            
            raise ValueError('View must return context dict or HTTP response')
        return wrapper
    return renderer


def check_permissions(*args, **kwargs):
    def renderer(func, *a, **k):
        def wrapper(request, *view_args, **view_kwargs):
            if 'admin_site' not in kwargs:
                admin_site = view_kwargs.get('modeladmin', None)
            if not admin_site or not admin_site.has_permission(request):
                return admin_site.login(request)
            return func(request, *view_args, **view_kwargs)
        return wrapper
    return renderer
