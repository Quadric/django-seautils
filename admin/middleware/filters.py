#coding: utf-8

from urlparse import urlparse

from django.http import QueryDict, HttpResponseRedirect
from django.core.urlresolvers import resolve

class KeepFiltersMiddleware:
    def process_request(self, request):
        ''' Django admin changelists filters keeper '''
        # Resolve current url to check if it is changelist
        resolved_url = resolve(request.path_info)
        if resolved_url.func.func_name != 'changelist_view':
            return 

        if request.META['QUERY_STRING']:
            # Put query into session, except pagination page number.
            # Query will be saved as dict.
            query = dict(request.GET.items())
            query.pop('p', None)
            query.pop('pop', None)
            request.session[request.path_info] = query
        else:
            # Request doesn't contain query string so check if it should be redirected
            if 'HTTP_REFERER' in request.META:
                # If the request comes from the same changelist
                # remove remembered query and pass request
                o = urlparse(request.META['HTTP_REFERER'])
                if o.path == request.path_info:
                    request.session[request.path_info] = None
                    return
            
            query = request.session.get(request.path_info, None)
            if query:
                finalquery = QueryDict('', mutable=True)
                finalquery.update(query)
                return HttpResponseRedirect(u'%s?%s' % (request.path_info,
                                                        finalquery.urlencode()))
