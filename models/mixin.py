#coding: utf-8
from django.contrib import admin
from django.core.urlresolvers import reverse


class ModelWithAdminUrls(object):

    @classmethod
    def get_cls(cls):
        return cls

    def get_obj(self):
        return self

    @classmethod
    def get_add_url(cls):
        c = cls.get_cls()
        return reverse('admin:%s_%s_add' % (c._meta.app_label, c._meta.module_name),
                       current_app=admin.site.name)

    def get_change_url(self):
        obj = self.get_obj()
        return reverse('admin:%s_%s_change' % (obj._meta.app_label, obj._meta.module_name),
                       args=(obj.id,), current_app=admin.site.name)

    @classmethod
    def get_changelist_url(cls):
        c = cls.get_cls()
        return reverse('admin:%s_%s_changelist' % (c._meta.app_label, c._meta.module_name),
                       current_app=admin.site.name)

    def get_delete_url(self):
        obj = self.get_obj()
        return reverse('admin:%s_%s_delete' % (obj._meta.app_label, obj._meta.module_name),
                       args=(obj.id,), current_app=admin.site.name)
