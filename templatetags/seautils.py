#coding: utf-8

from __future__ import absolute_import

import re

from django.template.defaultfilters import stringfilter
from django import template

register = template.Library()


@register.filter
def lookup(obj, key):
    if hasattr(obj, '__getitem__'):
        # is subscriptable
        try:
            return obj[key]
        except (IndexError, KeyError):
            return None
    # lookup for attribute
    return getattr(obj, str(key), None)

@register.filter
def paragraphs(value):
    """
    Turns paragraphs delineated with newline characters into
    paragraphs wrapped in <p> and </p> HTML tags.
    """
    paras = re.split(r'[\r\n]+', value)
    paras = ['<p>%s</p>' % p.strip() for p in paras]
    return '\n'.join(paras)
paragraphs = stringfilter(paragraphs)

